output "certificates" {
  value     = aws_acm_certificate.ssl_certificate
  sensitive = true
}
