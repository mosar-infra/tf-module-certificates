# certificates/variables.tf

variable "environment" {}
variable "certificate_domains" {}
variable "managed_by" {
  default = "certificates"
}
