# certificates/main.tf

resource "aws_acm_certificate" "ssl_certificate" {
  for_each          = var.certificate_domains
  domain_name       = each.value.domain
  validation_method = each.value.validation_method

  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }

  lifecycle {
    create_before_destroy = true
  }
}

